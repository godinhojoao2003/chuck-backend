import { Injectable } from '@nestjs/common';
import { ExternalError, ExternalResponse } from './../core/dtos/jokes.dtos';
import { PaginationInputDto } from './../core/dtos/pagination.dtos';
import { Repository } from './../core/interfaces/Repository';
@Injectable()
export class ChuckJokesService
  implements Repository<ExternalResponse | ExternalError>
{
  private async loadJokesByQuery(
    searchText: string,
  ): Promise<ExternalResponse | ExternalError> {
    const response = await fetch(
      `https://api.chucknorris.io/jokes/search?query=${searchText}`,
    );
    return response.json();
  }

  public async paginatedQuery(
    input: PaginationInputDto,
  ): Promise<ExternalResponse | ExternalError> {
    const { searchText } = input;
    const response = await this.loadJokesByQuery(searchText || '');
    if ('error' in response) {
      return response;
    }
    return {
      total: response.total,
      result: response.result,
    };
  }
}
