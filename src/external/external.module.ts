import { Module } from '@nestjs/common';
import { ChuckJokesService } from './chuck-jokes.service';
import { Repository } from './../core/interfaces/Repository';

@Module({
  providers: [
    {
      provide: Repository,
      useClass: ChuckJokesService,
    },
  ],
  exports: [Repository],
})
export class ExternalModule {}
