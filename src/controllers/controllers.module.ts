import { Module } from '@nestjs/common';
import { JokesResolver } from './jokes/jokes.resolver';
import { ExternalModule } from './../external/external.module';

@Module({
  providers: [JokesResolver],
  imports: [ExternalModule],
})
export class ControllersModule {}
