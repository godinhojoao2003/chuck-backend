import { Resolver, Query, Args } from '@nestjs/graphql';
import {
  JokeToViewDto,
  FindPaginatedJokesInput,
  PaginatedJokesToViewDto,
  ExternalResponse,
} from './../../core/dtos/jokes.dtos';
import { Repository } from './../../core/interfaces/Repository';
import { JokesMapper } from './../../core/mappers/jokes.mappers';
import { PaginationMapper } from './../../core/mappers/pagination.mappers';

@Resolver(() => JokeToViewDto)
export class JokesResolver {
  constructor(private readonly jokesRepository: Repository<ExternalResponse>) {}

  @Query(() => PaginatedJokesToViewDto)
  async findPaginatedJokes(
    @Args('input') input: FindPaginatedJokesInput,
  ): Promise<PaginatedJokesToViewDto> {
    const response = await this.jokesRepository.paginatedQuery(input);
    const mappedResult = JokesMapper.manyToView(response.result);
    return PaginationMapper.build(mappedResult, response.total);
  }
}
