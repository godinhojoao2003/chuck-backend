import { APP_PIPE } from '@nestjs/core';
import { Module, ValidationPipe } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver } from '@nestjs/apollo';
import { ControllersModule } from './controllers/controllers.module';
import {
  GraphqlError,
  ExternalGraphqlError,
} from './core/interfaces/GraphqlExceptions';

@Module({
  imports: [
    ControllersModule,
    GraphQLModule.forRoot({
      autoSchemaFile: 'src/schema.gql',
      driver: ApolloDriver,
      formatError: (error: ExternalGraphqlError) => {
        return new GraphqlError(error);
      },
      context: ({ req }) => ({ req }),
    }),
  ],
  providers: [
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe({
        transform: true,
      }),
    },
  ],
})
export class AppModule {}
