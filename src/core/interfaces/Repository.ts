import { PaginationInputDto } from '../dtos/pagination.dtos';

export abstract class Repository<T> {
  paginatedQuery: (input: PaginationInputDto) => Promise<T | null>;
}
