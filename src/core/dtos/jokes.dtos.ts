import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import { IsNotEmpty, Length } from 'class-validator';

export enum JokeCategoryEnum {
  Uncategorized = 'uncategorized',
  Animal = 'animal',
  Career = 'career',
  Dev = 'dev',
  Food = 'food',
  Music = 'music',
  Money = 'money',
  Movie = 'movie',
  Travel = 'travel',
}

export interface ExternalJokeDto {
  id: string;
  categories: JokeCategoryEnum[];
  created_at: string;
  updated_at: string;
  icon_url: string;
  url: string;
  value: string;
}

export interface ExternalError {
  timestamp: string;
  status: number;
  error: string;
  message: string;
}

export type ExternalResponse = { total: number; result: ExternalJokeDto[] };

@InputType()
export abstract class FindPaginatedJokesInput {
  @Field(() => Int)
  @IsNotEmpty()
  limit: number;

  @Field(() => Int)
  @IsNotEmpty()
  offset: number;

  @Field(() => String, { nullable: true })
  @IsNotEmpty()
  @Length(3, 120, { message: 'size must be between 3 and 120' })
  searchText?: string;
}

@ObjectType()
export abstract class JokeToViewDto {
  @Field()
  id: string;

  @Field({ nullable: true })
  category: JokeCategoryEnum;

  @Field()
  created_at: string;

  @Field()
  updated_at: string;

  @Field()
  icon_url: string;

  @Field()
  url: string;

  @Field()
  value: string;
}

@ObjectType()
export abstract class PaginatedJokesToViewDto {
  @Field(() => [JokeToViewDto])
  items: JokeToViewDto[];

  @Field(() => Int)
  totalCount: number;
}
