export interface PaginationInputDto {
  limit: number;
  offset: number;
  searchText?: string;
}

export interface PaginatedResultToViewDto<T> {
  items: T[];
  totalCount: number;
}
