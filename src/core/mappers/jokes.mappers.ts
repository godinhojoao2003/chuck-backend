import {
  ExternalJokeDto,
  JokeCategoryEnum,
  JokeToViewDto,
} from './../dtos/jokes.dtos';

export class JokesMapper {
  public static toView(dto: ExternalJokeDto): JokeToViewDto {
    return {
      id: dto.id + '-' + dto.created_at,
      category:
        (dto.categories && dto.categories[0]) || JokeCategoryEnum.Uncategorized,
      created_at: dto.created_at,
      updated_at: dto.updated_at,
      icon_url: dto.icon_url,
      url: dto.url,
      value: dto.value,
    };
  }

  public static manyToView(dtos: ExternalJokeDto[]): JokeToViewDto[] {
    return dtos.map((dto) => this.toView(dto));
  }
}
