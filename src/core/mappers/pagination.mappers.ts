import { PaginatedResultToViewDto } from '../dtos/pagination.dtos';

export class PaginationMapper {
  public static build<T>(
    data: T[],
    total: number,
  ): PaginatedResultToViewDto<T> {
    return {
      items: data,
      totalCount: total,
    };
  }
}
