import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';

describe('Jokes Resolver (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterAll(async () => await app.close());

  describe('findPaginatedJokes', () => {
    it('Given valid input should return paginated complaints [0]', async () => {
      const response = await request(app.getHttpServer())
        .post(`/graphql`)
        .send({
          query: `query findPaginatedJokes($input: FindPaginatedJokesInput!) {
            findPaginatedJokes(input: $input) {
              items {
                id
                category
                created_at
                updated_at
                icon_url
                url
                value
              }
              totalCount
            }
          }`,
          variables: {
            input: {
              offset: 1,
              limit: 1,
              searchText: 'Google Maps broke when searching for Chuck Norris.',
            },
          },
        });

      expect(response.statusCode).toBe(200);
      expect(response.body.data.findPaginatedJokes.totalCount).toBe(1);
      expect(response.body.data.findPaginatedJokes.items).toEqual([
        {
          id: 'ay_M9alERUe2dHseAIPx_Q-2020-01-05 13:42:18.823766',
          category: 'uncategorized',
          created_at: '2020-01-05 13:42:18.823766',
          updated_at: '2020-01-05 13:42:18.823766',
          icon_url: 'https://api.chucknorris.io/img/avatar/chuck-norris.png',
          url: 'https://api.chucknorris.io/jokes/ay_M9alERUe2dHseAIPx_Q',
          value: 'Google Maps broke when searching for Chuck Norris.',
        },
      ]);
    });

    it('Given invalid input with more than 120 chars at searchText field should return error', async () => {
      const response = await request(app.getHttpServer())
        .post(`/graphql`)
        .send({
          query: `query findPaginatedJokes($input: FindPaginatedJokesInput!) {
            findPaginatedJokes(input: $input) {
              items {
                id
                category
                created_at
                updated_at
                icon_url
                url
                value
              }
              totalCount
            }
          }`,
          variables: {
            input: {
              offset: 1,
              limit: 1,
              searchText:
                'Google Maps broke when searching for Chuck Norris.Google Maps broke when searching for Chuck Norris.Google Maps broke when searching for Chuck Norris.Google Maps broke when searching for Chuck Norris.Google Maps broke when searching for Chuck Norris.Google Maps broke when searching for Chuck Norris.',
            },
          },
        });

      expect(response.statusCode).toBe(200);
      expect(response.body.errors).toHaveLength(1);
      expect(response.body.errors[0].message).toBe('Bad Request Exception');
      expect(response.body.errors[0].code).toBe('BAD_REQUEST');
      expect(response.body.errors[0].path).toEqual(['findPaginatedJokes']);
      expect(response.body.errors[0].detailedMessage).toBe(
        'size must be between 3 and 120',
      );
      expect(response.body.data).toBeNull();
    });
  });
});
